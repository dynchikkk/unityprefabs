using UnityEngine;

public class RandomColorSetter : MonoBehaviour
{
    private Renderer _renderer;
    private MaterialPropertyBlock _materialPropertyBlock;

    void Start()
    {
        _renderer = GetComponent<Renderer>();
        _materialPropertyBlock = new();
    }

    void Update()
    {
        _materialPropertyBlock.SetColor("_Color", GetRandomColor(transform.position));
        _renderer.SetPropertyBlock(_materialPropertyBlock);
    }

    private Color GetRandomColor(Vector3 pos)
    {
        var color = new Color(pos.x + 0.5f, pos.y + 0.5f, pos.z + 0.5f, 1);
        return color;
    }
}
