using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace Boids
{
    [BurstCompile]
    public struct BoundsJob : IJobParallelFor
    {
        [ReadOnly] public NativeArray<Vector3> Positions;
        public NativeArray<Vector3> Accelerations;

        [ReadOnly] public Vector3 AreaSize;

        public void Execute(int index)
        {
            Vector3 pos = Positions[index];
            Vector3 size = AreaSize * 0.5f;
            Accelerations[index] += Compensate(-size.x - pos.x, Vector3.right)     /*Left side*/
                                    + Compensate(size.x - pos.x, Vector3.left)     /*Right side*/
                                    + Compensate(-size.y - pos.y, Vector3.up)      /*Down side*/
                                    + Compensate(size.y - pos.y, Vector3.down)     /*Top side*/
                                    + Compensate(-size.z - pos.z, Vector3.forward) /*Back side*/
                                    + Compensate(size.z - pos.z, Vector3.back);    /*Front side*/
        }

        private Vector3 Compensate(float delta, Vector3 direction)
        {
            const float treshold = 3f;
            const float multiplier = 100f;

            delta = math.abs(delta);
            if (delta > treshold)
                return Vector3.zero;
            return (1 - delta / treshold) * multiplier * direction;

        }
    }
}
