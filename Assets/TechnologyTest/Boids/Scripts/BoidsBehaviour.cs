using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;

namespace Boids
{
    public class BoidsBehaviour : MonoBehaviour
    {
        [Header("Base")]
        [SerializeField] private int _numberOfEntities = 10;
        [SerializeField] private GameObject _entityPrefab;

        [Header("Other")]
        [SerializeField] private float _destinationTreshold;
        [SerializeField, Range(1, 10)] private float _velocityLimit = 5;
        [SerializeField] private Vector3 _accelerationWeights;
        [SerializeField] private Vector3 _areaSize;


        private NativeArray<Vector3> _positions;
        private NativeArray<Vector3> _velocities;
        private NativeArray<Vector3> _accelerations;

        private TransformAccessArray _transformAccessArray;

        private void Start()
        {
            _positions = new NativeArray<Vector3>(_numberOfEntities, Allocator.Persistent);
            _velocities = new NativeArray<Vector3>(_numberOfEntities, Allocator.Persistent);
            _accelerations = new NativeArray<Vector3>(_numberOfEntities, Allocator.Persistent);

            var spawnedEntites = CreateEntities(_entityPrefab);
            _transformAccessArray = new TransformAccessArray(spawnedEntites);
        }

        private Transform[] CreateEntities(GameObject entity)
        {
            var transforms = new Transform[_numberOfEntities];
            for (int i = 0; i < _numberOfEntities; i++)
            {
                transforms[i] = Instantiate(entity, transform).transform;
                _velocities[i] = Random.insideUnitSphere;
            }

            return transforms;
        }

        private void Update()
        {
            MoveWork();
        }

        private void MoveWork()
        {
            var boundsJob = new BoundsJob()
            {
                Positions = _positions,
                Accelerations = _accelerations,
                AreaSize = _areaSize,
            };
            var accelerationJob = new AccelerationJob()
            {
                Positions = _positions,
                Velocities = _velocities,
                Accelerations = _accelerations,
                DestinationTreshold = _destinationTreshold,
                Weights = _accelerationWeights
            };
            var moveJob = new MoveJob()
            {
                Positions = _positions,
                Velocities = _velocities,
                Accelerations = _accelerations,
                DeltaTime = Time.deltaTime,
                VelocityLimit = _velocityLimit
            };

            var boundsHandle = boundsJob.Schedule(_numberOfEntities, 0);
            var accelerationHandle = accelerationJob.Schedule(_numberOfEntities, 0, boundsHandle);
            var moveHandle = moveJob.Schedule(_transformAccessArray, accelerationHandle);
            moveHandle.Complete();
        }


        private void OnDestroy()
        {
            DisposeArrays();
        }

        private void DisposeArrays()
        {
            _positions.Dispose();
            _velocities.Dispose();
            _accelerations.Dispose();
            _transformAccessArray.Dispose();
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(transform.position, _areaSize);
        }
    }
}
