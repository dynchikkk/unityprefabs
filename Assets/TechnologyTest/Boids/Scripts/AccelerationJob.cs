using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace Boids
{
    [BurstCompile]
    public struct AccelerationJob : IJobParallelFor
    {
        [ReadOnly] public NativeArray<Vector3> Positions;
        [ReadOnly] public NativeArray<Vector3> Velocities;
        public NativeArray<Vector3> Accelerations;

        public float DestinationTreshold;
        public Vector3 Weights;

        private int Count => Positions.Length - 1;

        public void Execute(int index)
        {
            Accelerations[index] += CalculateAceleration(index);
        }

        private Vector3 CalculateAceleration(int index)
        {
            Vector3 averageSpread = Vector3.zero,
                averageVelocity = Vector3.zero,
                averagePositions = Vector3.zero;

            for (int i = 0; i < Count; i++)
            {
                if (i == index)
                    continue;

                var targetPos = Positions[i];
                var posDifference = Positions[index] - targetPos;

                if (posDifference.magnitude > DestinationTreshold)
                    continue;
                averageSpread += posDifference.normalized;
                averageVelocity += Velocities[i];
                averagePositions += targetPos;
            }

            Vector3 acceleration = (averageSpread / Count) * Weights.x
                + (averageVelocity / Count) * Weights.y
                + (averagePositions / Count - Positions[index]) * Weights.z;

            return acceleration;
        }

    }
}
