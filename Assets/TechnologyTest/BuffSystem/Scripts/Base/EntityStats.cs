using UnityEngine;

namespace BuffTechnology
{
    public class EntityStats : ScriptableObject
    {
        public int Health = 100;
        public int Damage = 100;
        public bool IsImmortal = false;
    }
}
