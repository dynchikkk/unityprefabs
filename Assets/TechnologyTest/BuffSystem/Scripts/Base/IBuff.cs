﻿namespace BuffTechnology
{
    public interface IBuff
    {
        EntityStats ApplyBuff(EntityStats baseStats);
    }
}
