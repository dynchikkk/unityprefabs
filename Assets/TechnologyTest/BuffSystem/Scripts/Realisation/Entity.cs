using System.Collections.Generic;
using UnityEngine;

namespace BuffTechnology
{
    public class Entity : IBuffable
    {
        public EntityStats BaseStats { get; }
        public EntityStats CurrentStats { get; private set; }

        private readonly List<IBuff> _buffs = new List<IBuff>();

        public Entity(EntityStats baseStats)
        {
            BaseStats = baseStats;
            CurrentStats = baseStats;
        }

        public void AddBuff(IBuff buff)
        {
            _buffs.Add(buff);

            ApplyBuffs();

            Debug.Log($"{buff} applied, Stats: " +
                $"Damage: {CurrentStats.Damage}, " +
                $"Health: {CurrentStats.Health}, " +
                $"Immortal {CurrentStats.IsImmortal}");
        }

        public void RemoveBuff(IBuff buff)
        {
            _buffs.Remove(buff);

            ApplyBuffs();

            Debug.Log($"{buff} removed");
        }

        private void ApplyBuffs()
        {
            CurrentStats = ScriptableObject.Instantiate(BaseStats);

            foreach (IBuff buff in _buffs)
                CurrentStats = buff.ApplyBuff(CurrentStats);
        }
    }
}
