using BuffTechnology;
using UnityEngine;

public class DamageBuff : IBuff
{
    private readonly int _damageBonus = 0;

    public DamageBuff(int damageBonus)
    {
        _damageBonus = damageBonus;
    }

    public EntityStats ApplyBuff(EntityStats baseStats)
    {
        var newStats = baseStats;

        newStats.Damage = Mathf.Max(newStats.Damage + _damageBonus, 0);

        return newStats;
    }
}
