namespace BuffTechnology
{
    public class ImmortalityBuff : IBuff
    {
        public EntityStats ApplyBuff(EntityStats baseStats)
        {
            var newStats = baseStats;

            baseStats.IsImmortal = true;

            return newStats;
        }
    }
}
