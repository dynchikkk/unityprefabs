using UnityEngine;

namespace BuffTechnology
{
    public class EntityViewTest : MonoBehaviour
    {
        private Entity _character;

        private void Start()
        {
            Init(CreateCharacter());
        }


        private void Init(Entity character)
        {
            _character = character;

            Debug.Log($"Character created. "
                + $"Health: {_character.CurrentStats.Health},"
                + $"Damage: {_character.CurrentStats.Damage},"
                + $"IsImmortal: {_character.CurrentStats.IsImmortal}");
        }

        private Entity CreateCharacter()
        {
            var stats = ScriptableObject.CreateInstance<EntityStats>();
            stats.Health = 10;
            stats.Damage = 10;
            stats.IsImmortal = false;

            var character = new Entity(stats);
            return character;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                _character.AddBuff(new DamageBuff(10));

            if (Input.GetKeyDown(KeyCode.Alpha2))
                _character.AddBuff(new ImmortalityBuff());
        }

    }
}
