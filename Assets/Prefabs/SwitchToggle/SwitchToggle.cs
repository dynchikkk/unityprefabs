using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SwitchToggle : MonoBehaviour
{
    [SerializeField] RectTransform uiHandleRectTransform;
    [SerializeField] Color backgroundActiveColor;
    [SerializeField] Color handleActiveColor;

    [Header("DOTweenSpeed")]
    [SerializeField] private float _handleMoveSpeed = 0.3f;
    [SerializeField] private float _bgColorBlendSpeed = 0.6f;
    [SerializeField] private float _handleColorBlendSpeed = 0.3f;

    private Image _backgroundImage, handleImage;

    private Color backgroundDefaultColor, handleDefaultColor;

    private Toggle toggle;

    private Vector2 handlePosition;

    void Awake()
    {
        toggle = GetComponent<Toggle>();

        handlePosition = uiHandleRectTransform.anchoredPosition;

        _backgroundImage = uiHandleRectTransform.parent.GetComponent<Image>();
        handleImage = uiHandleRectTransform.GetComponent<Image>();

        backgroundDefaultColor = _backgroundImage.color;
        handleDefaultColor = handleImage.color;

        toggle.onValueChanged.AddListener(OnSwitch);

        if (toggle.isOn)
            OnSwitch(true);
    }

    void OnSwitch(bool on)
    {
        //uiHandleRectTransform.anchoredPosition = on ? handlePosition * -1 : handlePosition ; // no anim
        uiHandleRectTransform.DOAnchorPos(on ? handlePosition * -1 : handlePosition, _handleMoveSpeed).SetEase(Ease.InOutFlash);

        //backgroundImage.color = on ? backgroundActiveColor : backgroundDefaultColor ; // no anim
        _backgroundImage.DOColor(on ? backgroundActiveColor : backgroundDefaultColor, _bgColorBlendSpeed);

        //handleImage.color = on ? handleActiveColor : handleDefaultColor ; // no anim
        handleImage.DOColor(on ? handleActiveColor : handleDefaultColor, _handleColorBlendSpeed);
    }

    void OnDestroy()
    {
        toggle.onValueChanged.RemoveListener(OnSwitch);
    }
}