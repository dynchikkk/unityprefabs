using System;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Character : MonoBehaviour, IMovable, IJumpable
{
    public event Action<Vector3> OnMove;
    public event Action<Vector3> OnJump;

    public IMovement Movement { get; private set; }

    [Header("Base attribute")]
    [SerializeField] private float _gravity = -9.81f;

    [Header("Move attribute")]
    [SerializeField] private float _speed = 5f;

    [Header("Jump attrubute")]
    [SerializeField] private float _jumpHeight = 3f;
    [SerializeField] private float _checkGroundRadius = 0.4f;
    [SerializeField] private Transform _groundCheckerPivot;
    [SerializeField] private LayerMask _groundMask;

    private Vector3 _moveDirection = Vector3.zero;
    private float _velocity;
    private bool _isGrounded = false;
    private CharacterController _controller;

    public void Init()
    {
        _controller = GetComponent<CharacterController>();
        Movement = GetComponent<IMovement>();
        if (Movement == null)
            throw new System.Exception("No Movement");
        Movement.Init();
    }

    private void FixedUpdate()
    {
        _isGrounded = IsOnTheGround();

        if (_isGrounded && _velocity < 0)
            _velocity = -2;

        MoveInternal();
        DoGravity();
    }

    #region Move
    public void Move(Vector3 direction) =>
        _moveDirection = direction;

    private void MoveInternal()
    {
        Vector3 motion = _moveDirection * Time.fixedDeltaTime * _speed;
        if (motion.magnitude == 0)
            return;

        _controller.Move(motion);
        OnMove?.Invoke(transform.position);
    }
    #endregion

    #region Jump
    public void Jump()
    {
        if (!_isGrounded)
            return;

        _velocity = Mathf.Sqrt(_jumpHeight * -2 * _gravity);
        OnJump?.Invoke(transform.position);
    }

    private bool IsOnTheGround()
    {
        bool result = Physics.CheckSphere(_groundCheckerPivot.position, _checkGroundRadius, _groundMask);
        return result;
    }
    #endregion

    private void DoGravity()
    {
        _velocity += _gravity * Time.fixedDeltaTime;
        _controller.Move(Vector3.up * _velocity * Time.fixedDeltaTime);
    }
}

