using System;
using UnityEngine;

public class FirstPersonCharacterMovementLight : MonoBehaviour, IMovement
{
    private const string HORIZONTAL = "Horizontal";
    private const string VERTICAL = "Vertical";

    public event Action OnMove;

    [SerializeField] private float _rotateSpeed = 10f;

    public bool CanMove { get; private set; }
    private IMovable _movable;
    private IJumpable _jumpable;
    private Camera _playerCamera;

    public void Init()
    {
        _movable = GetComponent<IMovable>();
        _jumpable = GetComponent<IJumpable>();
        _playerCamera = GetComponentInChildren<Camera>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (!CanMove)
            return;

        RotateAndLook();
        ReadMove();
        ReadJump();
    }

    public void SetMoveCondition(bool cond) =>
        CanMove = cond;

    private void ReadMove()
    {
        var horizontal = Input.GetAxis(HORIZONTAL);
        var vertical = Input.GetAxis(VERTICAL);
        Vector3 direction = new Vector3(horizontal, 0, vertical);
        direction = transform.TransformDirection(direction);

        _movable.Move(direction);
    }

    private void ReadJump()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            _jumpable.Jump();
            OnMove?.Invoke();
        }
    }

    private void RotateAndLook()
    {
        transform.Rotate(0, Input.GetAxis("Mouse X") * _rotateSpeed, 0);

        _playerCamera.transform.Rotate(-Input.GetAxis("Mouse Y") * _rotateSpeed, 0, 0);
        if (_playerCamera.transform.localRotation.eulerAngles.y != 0)
        {
            _playerCamera.transform.Rotate(Input.GetAxis("Mouse Y") * _rotateSpeed, 0, 0);
        }
    }
}
