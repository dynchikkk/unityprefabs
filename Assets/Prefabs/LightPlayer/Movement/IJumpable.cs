using System;
using UnityEngine;

public interface IJumpable
{
    event Action<Vector3> OnJump;
    void Jump();
}
