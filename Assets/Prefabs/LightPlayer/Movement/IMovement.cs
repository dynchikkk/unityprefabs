public interface IMovement
{
    bool CanMove { get; }
    void Init();
    void SetMoveCondition(bool cond);
}
