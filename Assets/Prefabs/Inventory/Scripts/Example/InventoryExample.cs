using System.Collections.Generic;
using UnityEngine;

namespace Inventory
{
    public class InventoryExample : MonoBehaviour
    {
        private void Start()
        {
            var config = new InventoryConfig
            {
                InventorySize = new Vector2Int(3, 4),
                InventorySlotCapacity = 99
            };

            var inventorySize = config.InventorySize.x * config.InventorySize.y;
            var inventory = new InventoryData
            {
                Slots = new List<InventorySlotData>(inventorySize)
            };

            for (int i = 0; i < inventorySize; i++)
                inventory.Slots.Add(new InventorySlotData());

            var inventoryService = new InventoryService(inventory, config);

            inventoryService.PrintInventory();

            inventoryService.Add("oooItem", 15);
            inventoryService.Add("oooItem", 89);
            inventoryService.Add("eeeItem", 53);

            inventoryService.PrintInventory();
        }
    }
}
