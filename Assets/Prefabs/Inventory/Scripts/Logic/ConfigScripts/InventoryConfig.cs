using UnityEngine;

namespace Inventory
{
    public class InventoryConfig
    {
        public Vector2Int InventorySize;
        public int InventorySlotCapacity;
    }
}
