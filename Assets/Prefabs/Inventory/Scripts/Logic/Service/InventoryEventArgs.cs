using UnityEngine;

namespace Inventory
{
    public struct InventoryEventArgs
    {
        public string ItemId { get; }
        public int Amount { get; }
        public Vector2Int InventorySlotsCoordinates { get; }

        public InventoryEventArgs(string itemId, int amount, Vector2Int inventorySlotsCoordinates)
        {
            ItemId = itemId;
            Amount = amount;
            InventorySlotsCoordinates = inventorySlotsCoordinates;
        }
    }
}
