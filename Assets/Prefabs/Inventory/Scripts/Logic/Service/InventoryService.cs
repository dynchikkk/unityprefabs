using System;
using System.Linq;
using UnityEngine;

namespace Inventory
{
    public class InventoryService
    {
        public event Action<InventoryEventArgs> OnItemsAdded;
        public event Action<InventoryEventArgs> OnItemsRemoved;
        public event Action<string, int> OnItemsDropped;

        private readonly InventoryData _inventoryData;
        private readonly InventoryConfig _config;

        public InventoryService(InventoryData invenrotyData, InventoryConfig config)
        {
            _inventoryData = invenrotyData;
            _config = config;
        }

        public void Add(string itemId, int amount = 1)
        {
            var remainingAmount = amount;

            AddToSlotWithSameItems(itemId, remainingAmount, out remainingAmount);

            if (remainingAmount <= 0)
                return;

            AddToFirstAvailableSlot(itemId, remainingAmount, out remainingAmount);

            if (remainingAmount > 0)
                InvokeDrop(itemId, remainingAmount);

        }

        public void Add(Vector2Int slotCoordinates, string itemId, int amount = 1)
        {
            var rowLength = _config.InventorySize.x;
            var slotIndex = slotCoordinates.x + rowLength * slotCoordinates.y;
            var slot = _inventoryData.Slots[slotIndex];
            var newAmount = slot.Amount + amount;

            if (slot.IsEmpty())
                slot.ItemId = itemId;

            if (newAmount > _config.InventorySlotCapacity)
            {
                var remainingItems = newAmount - _config.InventorySlotCapacity;
                var itemsToAddAmount = _config.InventorySlotCapacity - slot.Amount;
                slot.Amount = _config.InventorySlotCapacity;

                OnItemsAdded?.Invoke(new InventoryEventArgs(itemId, itemsToAddAmount, slotCoordinates));

                Add(itemId, remainingItems);
            }
            else
            {
                slot.Amount = newAmount;
                OnItemsAdded?.Invoke(new InventoryEventArgs(itemId, amount, slotCoordinates));
            }
        }

        public bool Remove(string itemId, int amount = 1, bool invokeDrop = true)
        {
            if (!Has(itemId, amount))
                return false;

            var amountToRemove = amount;
            var size = _config.InventorySize;
            var rowLength = size.x;

            for (int x = 0; x < size.x; x++)
            {
                for (int y = 0; y < size.y; y++)
                {
                    var slotCoords = new Vector2Int(x, y);
                    var slot = _inventoryData.Slots[slotCoords.x + rowLength * slotCoords.y];

                    if (slot.ItemId != itemId)
                        continue;

                    if (amountToRemove > slot.Amount)
                    {
                        amountToRemove -= slot.Amount;
                        Remove(slotCoords, itemId, slot.Amount, invokeDrop);
                    }
                    else
                    {
                        Remove(slotCoords, itemId, amountToRemove, invokeDrop);
                        return true;
                    }
                }
            }

            return true;
        }

        public bool Remove(Vector2Int slotCoordinates, string itemId, int amount = 1, bool invokeDrop = true)
        {
            var size = _config.InventorySize;
            var rowLength = size.x;
            var slot = _inventoryData.Slots[slotCoordinates.x + rowLength * slotCoordinates.y];

            if (slot.IsEmpty() || slot.ItemId != itemId || slot.Amount < amount)
                return false;

            slot.Amount -= amount;

            if (slot.Amount == 0)
                slot.Clean();

            OnItemsRemoved?.Invoke(new InventoryEventArgs(itemId, amount, slotCoordinates));

            if (invokeDrop)
                InvokeDrop(itemId, amount);

            return true;
        }

        public bool Has(string itemId, int amount = 1)
        {
            var allSlotsWithItem = _inventoryData.Slots.Where(s => s.ItemId == itemId);
            var sumExists = 0;

            foreach (var slot in allSlotsWithItem)
                sumExists += slot.Amount;

            return sumExists >= amount;
        }

        private void AddToSlotWithSameItems(string itemId, int amount, out int remainingAmount)
        {
            var size = _config.InventorySize;
            var rowLength = size.x;
            remainingAmount = amount;

            for (int x = 0; x < size.x; x++)
            {
                for (int y = 0; y < size.y; y++)
                {
                    var coords = new Vector2Int(x, y);
                    var slot = _inventoryData.Slots[coords.x + rowLength * coords.y];

                    if (slot.IsEmpty())
                        continue;

                    if (slot.Amount >= _config.InventorySlotCapacity)
                        continue;

                    if (slot.ItemId != itemId)
                        continue;

                    var newAmount = slot.Amount + remainingAmount;

                    if (newAmount > _config.InventorySlotCapacity)
                    {
                        remainingAmount = newAmount - _config.InventorySlotCapacity;
                        var itemsToAddAmount = _config.InventorySlotCapacity - slot.Amount;
                        slot.Amount = _config.InventorySlotCapacity;

                        OnItemsAdded?.Invoke(new InventoryEventArgs(itemId, itemsToAddAmount, coords));
                    }
                    else
                    {
                        slot.Amount = newAmount;
                        var itemsToAddAmount = remainingAmount;
                        remainingAmount = 0;

                        OnItemsAdded?.Invoke(new InventoryEventArgs(itemId, itemsToAddAmount, coords));
                        return;
                    }
                }
            }
        }

        private void AddToFirstAvailableSlot(string itemId, int amount, out int remainingAmount)
        {
            var size = _config.InventorySize;
            var rowLength = size.x;
            remainingAmount = amount;

            for (int x = 0; x < size.x; x++)
            {
                for (int y = 0; y < size.y; y++)
                {
                    var coords = new Vector2Int(x, y);
                    var slot = _inventoryData.Slots[coords.x + rowLength * coords.y];

                    if (!slot.IsEmpty())
                        continue;

                    slot.ItemId = itemId;
                    var newAmount = remainingAmount;

                    if (newAmount > _config.InventorySlotCapacity)
                    {
                        remainingAmount = newAmount - _config.InventorySlotCapacity;
                        var itemsToAddAmount = _config.InventorySlotCapacity;
                        slot.Amount = _config.InventorySlotCapacity;

                        OnItemsAdded?.Invoke(new InventoryEventArgs(itemId, itemsToAddAmount, coords));
                    }
                    else
                    {
                        slot.Amount = newAmount;
                        var itemsToAddAmount = remainingAmount;
                        remainingAmount = 0;

                        OnItemsAdded?.Invoke(new InventoryEventArgs(itemId, itemsToAddAmount, coords));
                        return;
                    }
                }
            }
        }

        private void InvokeDrop(string itemId, int amount = 1) =>
            OnItemsDropped?.Invoke(itemId, amount);

        public void PrintInventory()
        {
            var line = "";
            var size = _config.InventorySize;
            var rowLength = size.x;

            for (var x = 0; x < size.x; x++)
            {
                for (int y = 0; y < size.y; y++)
                {
                    var coords = new Vector2Int(x, y);
                    var slot = _inventoryData.Slots[coords.x + rowLength * coords.y];

                    line += $"Slot ({x} {y}): ItemId = {slot.ItemId}, amount = {slot.Amount} ";

                }

                line += "\n";
            }

            Debug.Log(line);
        }
    }
}
