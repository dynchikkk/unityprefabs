
namespace Inventory
{
    public static class InventoryExtensions
    {
        public static bool IsEmpty(this InventorySlotData slot)
        {
            return slot.Amount <= 0 || string.IsNullOrEmpty(slot.ItemId);
        }

        public static void Clean(this InventorySlotData slot)
        {
            slot.Amount = 0;
            slot.ItemId = null;
        }

    }
}
