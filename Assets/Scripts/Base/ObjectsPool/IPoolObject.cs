using System;

namespace Pool
{
    public interface IPoolObject
    {
        event Action<IPoolObject> OnObjectNeededToDeactivate;
        void ResetBeforeBackToPool();
    }
}
